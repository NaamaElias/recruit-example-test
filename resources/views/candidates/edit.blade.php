@extends('layouts.app')

@section('title', 'Edit candidate')

@section('content') 

<h1>Edit Candidate</h1>
<form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
    @csrf <!-- Security -->
    @METHOD('PATCH')
    <div class="form-group">
        <label for = "name">Candidate Name</label>
        <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}> <!-- lable for = "X" , name = "X" -->
    </div>
    <div class="form-group">
        <label for = "email">Candidate Email</label>
        <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}> 
    </div>
    <div>
        <input type = "submit" name = "submit" value = "Update Candidate">
    </div>
</form>       
@endsection