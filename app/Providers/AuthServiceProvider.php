<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // check if user can change candidate's status (only user that belongs to the candidate can do it)
        // Gate take to $user the user that log-in to the system authomaticly
        Gate::define('change-status', function ($user, $candidate) {
            return $user->id === $candidate->user_id;
        });

        // check if user can change the user (only manager can)
        Gate::define('assign-user', function ($user) {
            return $user->isManager();
        });
    }
}
