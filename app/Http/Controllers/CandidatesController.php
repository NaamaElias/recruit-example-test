<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

// full name is "App\Http\Controllers\CandidatesController";
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // select * from candidate table
        $candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses'));
    } 

    public function myCandidates()
    {
        $userId = Auth::id(); //bring the current user_id
        $user = User::findOrFail($userId);
        $candidates = $user->candidates;  // SELECT candidates WHERE user_id=$userId
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses'));
    }

    public function changeUser($cid,$uid = null){
        Gate::authorize('assign-user');
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        return back();
        //return redirect('candidates'); // send to index function to reload the candidates table
    }


    public function changeStatus($cid,$sid){
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status',$candidate))
        {
            $from = $candidate->status->id;
            if(!Status::allowed($from,$sid)) return redirect('candidates');
            $candidate->status_id = $sid;
            $candidate->save();
        }else{
            Session::flash('notallowed','You are not allowed to change the status of the user because you are not the owner of the user');  
        }
        return back();
        //return redirect('candidates'); // send to index function to reload the candidates table
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate = new Candidate(); // create empty row from the candidate model
        //$candidate->name = $request->name; 
        //$candidate->email = $request->email;
        $candidate->create($request->all()); //another way to store name and email (by mass assignment)
        return redirect('candidates'); // send the user back to candidates page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id); // get id from database
        return view('candidates.edit', compact('candidate')); //give a candidate to edit form

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // request - the information from PATCH
        $candidate = Candidate::findOrFail($id); //pull the candidate from the database and check findOrFail
        $candidate->update($request->all()); //update evry change by mass assignment
        // we need to allow mass assignment inside the candidate model (by fillable)
        return redirect('candidates');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');

    }
}
